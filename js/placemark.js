ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [59.963631, 30.301801],
            zoom: 15
        }, {
            searchControlProvider: 'yandex#search'
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [59.963631, 30.301801]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: '',
                hintContent: ''
            }
        });

    myMap.geoObjects.add(myGeoObject);

  // myPlacemark.events
  //   .add('mouseenter', function (e) {
  //     // Ссылку на объект, вызвавший событие,
  //     // можно получить из поля 'target'.
  //     e.get('target').options.set('preset', 'islands#greenIcon');
  //   })
  //   .add('mouseleave', function (e) {
  //     e.get('target').options.unset('preset');
  //   });
}
